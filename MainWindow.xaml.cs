﻿using Binarysharp.MemoryManagement;
using Binarysharp.MemoryManagement.Modules;
using Binarysharp.MemoryManagement.Patterns;
using CefSharp;
using CefSharp.SchemeHandler;
using CefSharp.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using XinTracker.Zelda;
using XinTracker.Zelda.OcarinaOfTime;

namespace XinTracker
{
    public partial class MainWindow : Window
    {
        private Dictionary<string, string> themes;
        private string currentTheme;

        public IZeldaSaveData Zelda;
        public event Action GameDataChanged;
        public int SaveSlot { get; protected set; }

        private Timer DataUpdater;

        public MainWindow()
        {
            this.SizeToContent = SizeToContent.WidthAndHeight;
            var settings = new CefSettings();

            settings.RegisterScheme(new CefCustomScheme
            {
                SchemeName = "tracker",
                SchemeHandlerFactory = new FolderSchemeHandlerFactory(Directory.GetCurrentDirectory())
            });

            Cef.Initialize(settings);

            this.InitializeComponent();

            this.Browser.JavascriptObjectRepository.Register("XinTracker", new XinTrackerJS(this), false);
            this.Browser.JavascriptObjectRepository.Register("ItemHelper", new Items(), false);

            this.themes = new Dictionary<string, string>();
            var dirs = Directory.EnumerateDirectories(Directory.GetCurrentDirectory());
            foreach(var dir in dirs)
                foreach(var file in Directory.EnumerateFiles(dir))
                    if(file.Split('\\').Last().StartsWith("index.html"))
                        this.themes.Add(dir.Split('\\').Last(), dir);

            this.currentTheme = this.themes.First().Value;
            this.Browser.FrameLoadEnd += this.SetInitialTheme;

            this.MenuThemes.Visibility = Visibility.Hidden;
            if(this.themes.Count > 1)
            {
                this.MenuThemes.Visibility = Visibility.Visible;
                foreach(var theme in this.themes)
                {
                    var menuItem = new MenuItem();
                    menuItem.Header = theme.Key;
                    menuItem.Click += this.ChangeTheme;
                    this.MenuThemes.Items.Add(menuItem);
                }
            }

            this.Browser.Width = 600;
            this.Browser.Height = 650;

            this.DataUpdater = new Timer(this.UpdateZelda, this, 0, 400);
        }

        private void UpdateZelda(object state)
        {
            if(this.Zelda == null)
                return;

            if(!this.Zelda.UpdateData())
                return;

            this.Dispatcher.InvokeAsync(() => { this.UpdateSaveNames(); });

            GameDataChanged();
        }

        private void UpdateSaveNames()
        {
            this.MenuSaves.Visibility = Visibility.Hidden;
            for(int i = 0; i < 3; i++)
            {
                ((MenuItem)this.MenuSaves.Items[i]).Header = this.Zelda[i].Name;
                if(this.Zelda[i].Name != "00000000")
                    this.MenuSaves.Visibility = Visibility.Visible;
            }
        }

        private void ChangeTheme(object sender, RoutedEventArgs e)
        {
            if(!(e.Source is MenuItem))
                return;

            var menuItem = (MenuItem)e.Source;
            this.currentTheme = this.themes[(string)menuItem.Header];
            this.Browser.Load(this.currentTheme + "\\index.html");
            this.Browser.ExecuteScriptAsync("gameDataUpdate()");
        }

        private void SetInitialTheme(object sender, EventArgs e)
        {
            this.Browser.Load(this.currentTheme + "\\index.html");
            this.Browser.FrameLoadEnd -= this.SetInitialTheme;
        }

        private void ScanMemoryClickHandler(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Beginning memory search for Zelda...");
            IEnumerable<Process> processes = Process.GetProcesses();

            string[] emulators = { "retroarch", "emuhawk" };
            string[] knownModules = { "para", "mupen" };

            List<Process> matched = new List<Process>();
            foreach(var process in processes)
            {
                Console.WriteLine("Detected Process " + process.ProcessName);
                foreach(var emu in emulators)
                    if(process.ProcessName.ToLower().Contains(emu))
                        matched.Add(process);
            }

            foreach(var process in matched)
            {
                Console.WriteLine("Scanning process " + process.ProcessName);
                var memorySharp = new MemorySharp(process);
                List<RemoteModule> modules = new List<RemoteModule>();
                modules.Add(memorySharp.Modules.MainModule);
                foreach(var module in memorySharp.Modules.RemoteModules)
                    modules.Add(module);

                foreach(var module in modules)
                {
                    bool shouldScan = false;
                    foreach(var mod in knownModules)
                    {
                        if(module.Name.ToLower().Contains(mod))
                        {
                            shouldScan = true;
                            break;
                        }
                    }

                    if(!shouldScan)
                        continue;

                    Console.WriteLine("Scanning module " + module.Name + "...");

                    var scanner = new PatternScanner(module);
                    PatternScanResult result = new PatternScanResult();
                    result.Offset = 0;

                    result = scanner.Find(new SaveReaderMemory.ROMStringPattern());

                    if(result.ScanWasSuccessful)
                    {
                        this.Zelda = new SaveReaderMemory(memorySharp, process, module, result.Offset);
                        return;
                    }
                }
            }
        }

        private void SelectSRAMClickHandler(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "C:\\";
            openFileDialog.Filter = "SRAM files (*.sra)|*.sra|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 0;
            openFileDialog.RestoreDirectory = true;

            if(openFileDialog.ShowDialog() == true)
            {
                Zelda = new SaveReaderFile(openFileDialog.FileName);
                return;
            }
        }

        private void SelectROMClickHandler(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Loading of ROMs is not yet supported, but is coming in the future!  This will allow the tracker to read much more about your game, such as key usage, remaining chests and events, and other cool things like that.  I might even put some groovy 'cheaty' stuff in.");
        }

        private void SelectSaveSlot(object sender, RoutedEventArgs e)
        {
            this.SaveSlot = 0;
            for(int i = 0; i < 3; i++)
                if(this.MenuSaves.Items[i] == sender)
                    this.SaveSlot = i;
            this.GameDataChanged();
        }

        public void CallRefreshGameData() => this.Browser.ExecuteScriptAsync("refreshGameData()");
    }
}
