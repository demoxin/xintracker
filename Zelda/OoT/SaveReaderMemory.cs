﻿using Binarysharp.MemoryManagement;
using Binarysharp.MemoryManagement.Modules;
using Binarysharp.MemoryManagement.Patterns;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace XinTracker.Zelda.OcarinaOfTime
{
    public class SaveReaderMemory : IZeldaSaveData
    {
        private MemorySharp reader;
        public Process Process { get; protected set; }
        public RemoteModule Module { get; protected set; }
        public int Offset;
        public byte[] Data { get; protected set; }

        private SaveData[] saves;

        public SaveReaderMemory(MemorySharp reader, Process process, RemoteModule module, int v)
        {
            this.reader = reader;
            this.Process = process;
            this.Module = module;
            this.Offset = v;

            this.Data = new byte[0x8000];

            this.saves = new SaveData[3];
            this.saves[0] = new SaveData(this, 0);
            this.saves[1] = new SaveData(this, 1);
            this.saves[2] = new SaveData(this, 2);
        }

        public bool UpdateData()
        {
            var oldData = this.Data;
            this.Data = this.Module.Read<byte>(this.Offset, 0x8000);
            byte b0, b1, b2, b3;
            for(int i = 0; i < this.Data.Length; i += 4)
            {
                b0 = this.Data[i];
                b1 = this.Data[i + 1];
                b2 = this.Data[i + 2];
                b3 = this.Data[i + 3];
                this.Data[i] = b3;
                this.Data[i + 1] = b2;
                this.Data[i + 2] = b1;
                this.Data[i + 3] = b0;
            }

            return !this.Data.SequenceEqual(oldData);
        }

        public SaveData this[int key] { get => this.saves[key]; }

        public byte SoundOptions { get => this.Module.Read<byte>(this.Offset); }
        public byte ZTargetOptions { get => this.Module.Read<byte>(this.Offset + 0x01); }
        public byte LanguageOptions { get => this.Module.Read<byte>(this.Offset + 0x02); }

        public class ROMStringPattern : IMemoryPattern
        {
            public int Offset { get => 0; }

            private byte[] bytePattern;
            private string patternMask;

            public ROMStringPattern()
            {
                this.bytePattern = new byte[68];
                var header = new byte[] { 0x98, 0x00, 0x00, 0x00, 0x5A, 0x21, 0x10, 0x09, 0x41, 0x44, 0x4C, 0x45 };
                Array.Copy(header, 0, this.bytePattern, 0, 12);
                var save = new byte[] { 0x44, 0x4C, 0x45, 0x5A, 0x00, 0x00, 0x5A, 0x41 };
                Array.Copy(save, 0, this.bytePattern, 60, 8);

                this.patternMask = "";

                foreach(var byt in this.bytePattern)
                {
                    if(byt != 0)
                        this.patternMask += "x";
                    else
                        this.patternMask += "?";
                }
            }

            public MemoryPatternType PatternType { get => MemoryPatternType.Data; }

            public IList<byte> GetBytes() => this.bytePattern;
            public string GetMask() => this.patternMask;
        }
    }
}