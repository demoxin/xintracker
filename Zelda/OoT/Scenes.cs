﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XinTracker.Zelda.OcarinaOfTime
{
    public struct Scenes
    {
        static readonly byte DekuTree = 0x0;
        static readonly byte DodongosCavern = 0x1;
        static readonly byte JabuJabu = 0x2;
        static readonly byte BottomOfTheWell = 0x8;

        static readonly byte ForestTemple = 0x3;
        static readonly byte FireTemple = 0x4;
        static readonly byte WaterTemple = 0x5;
        static readonly byte ShadowTemple = 0x7;
        static readonly byte SpiritTemple = 0x6;
        static readonly byte GerudoFortress = 0xC;
        static readonly byte GerudoTrainingGrounds = 0xB;
        static readonly byte GanonsCastle = 0xD;

    }
}
