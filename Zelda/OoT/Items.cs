﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XinTracker.Zelda.OcarinaOfTime
{
    public struct Items
    {
        public static readonly byte DekuSticks = 0x0;
        public static readonly byte DekuNuts = 0x1;
        public static readonly byte Bombs = 0x2;
        public static readonly byte Bow = 0x3;
        public static readonly byte FireArrows = 0x4;
        public static readonly byte DinsFire = 0x5;
        public static readonly byte Slingshot = 0x6;
        public static readonly byte FairyOcarina = 0x7;
        public static readonly byte OcarinaOfTime = 0x8;
        public static readonly byte Bombchus = 0x9;
        public static readonly byte Hookshot = 0xA;
        public static readonly byte Longshot = 0xB;
        public static readonly byte IceArrows = 0xC;
        public static readonly byte FaroresWind = 0xD;
        public static readonly byte Boomerang = 0xE;
        public static readonly byte LensOfTruth = 0xF;
        public static readonly byte MagicBeans = 0x10;
        public static readonly byte MegatonHammer = 0x11;
        public static readonly byte LightArrows = 0x12;
        public static readonly byte NayrusLove = 0x13;
        public static readonly byte EmptyBottle = 0x14;
        public static readonly byte RedPotion = 0x15;
        public static readonly byte GreenPotion = 0x16;
        public static readonly byte BluePotion = 0x17;
        public static readonly byte BottledFairy = 0x18;
        public static readonly byte BottledFish = 0x19;
        public static readonly byte LonLonMilk = 0x1A;
        public static readonly byte RutosLetter = 0x1B;
        public static readonly byte BlueFire = 0x1C;
        public static readonly byte Bugs = 0x1D;
        public static readonly byte BigPoe = 0x1E;
        public static readonly byte LonLonMilkHalf = 0x1F;
        public static readonly byte Poe = 0x20;
        public static readonly byte WeirdEgg = 0x21;
        public static readonly byte Cucco = 0x22;
        public static readonly byte ZeldasLetter = 0x23;
        public static readonly byte KeatonMask = 0x24;
        public static readonly byte SkullMask = 0x25;
        public static readonly byte SpookyMask = 0x26;
        public static readonly byte BunnyHood = 0x27;
        public static readonly byte GoronMask = 0x28;
        public static readonly byte ZoraMask = 0x29;
        public static readonly byte GerudoMask = 0x2A;
        public static readonly byte MaskOfTruth = 0x2B;
        public static readonly byte SoldOut = 0x2C;
        public static readonly byte PocketEgg = 0x2D;
        public static readonly byte PocketCucco = 0x2E;
        public static readonly byte Cojiro = 0x2F;
        public static readonly byte OddMushroom = 0x30;
        public static readonly byte OddPotion = 0x31;
        public static readonly byte PoachersSaw = 0x32;
        public static readonly byte BrokenGoronSword = 0x33;
        public static readonly byte Prescription = 0x34;
        public static readonly byte EyeballFrog = 0x35;
        public static readonly byte EyeDrops = 0x36;
        public static readonly byte ClaimCheck = 0x37;

        public static string GetName(byte itemID)
        {
            foreach(var item in typeof(Items).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                if((byte)item.GetValue(null) == itemID)
                    return item.Name;
            return "Unknown";
        }

        public string GetItemName(int itemID)
        {
            foreach(var item in typeof(Items).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                if((byte)item.GetValue(null) == itemID)
                    return item.Name;
            return "Unknown";
        }

        public int GetItemID(string itemName)
        {
            foreach(var item in typeof(Items).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
            {
                if(item.Name.ToLower().Equals(itemName.ToLower()))
                    return (int)(byte)item.GetValue(null);
            }
            return -1;
        }
    }
}
