﻿using System;
using System.Collections.Generic;

namespace XinTracker.Zelda.OcarinaOfTime
{
    public class SaveData
    {
        private IZeldaSaveData reader;
        private ushort entry;

        private int offset { get => (0x0020) + (this.entry * 0x1450); }

        public SaveData(IZeldaSaveData reader, ushort entry)
        {
            this.reader = reader;
            this.entry = entry;
        }

        public string Name
        {
            get
            {
                byte[] bytes = new byte[8];
                Array.Copy(this.reader.Data, this.offset + 0x0024, bytes, 0, 8);

                for(int i = 0; i < bytes.Length; i++)
                {
                    if(bytes[i] == 0xDF)
                        bytes[i] = 0;
                    else
                        bytes[i] = ((byte)(bytes[i] - (byte)0x6A));
                }

                return ZeldaString.Build(bytes);

            }
        }

        public short WorldTime { get => BitConverter.ToInt16(this.reader.Data, this.offset + 0x000C); }

        public short Deaths { get => BitConverter.ToInt16(this.reader.Data, this.offset + 0x0022); }

        public double MaxHP { get => (BitConverter.ToUInt16(this.reader.Data, this.offset + 0x002E) / 1024) * 0.25; }
        public double CurrentHP { get => (BitConverter.ToUInt16(this.reader.Data, this.offset + 0x0030) / 1024) * 0.25; }

        public byte MaxMagic { get => (byte)(this.reader.Data[this.offset + 0x0032] * 48); }
        public byte CurrentMagic { get => this.reader.Data[this.offset + 0x0033]; }
        public byte MagicMeterLevel { get => this.reader.Data[this.offset + 0x0032]; }

        public ushort Rupees { get => (ushort)(BitConverter.ToUInt16(this.reader.Data, this.offset + 0x0034)); }

        public override string ToString()
        {
            var ret = "Name: " + this.Name + "\n";
            ret += "Rupees: " + this.Rupees + "/" + this.RupeeCapacity + "\n";
            if(this.DoubleDefense)
                ret += "**";
            ret += "Health: " + this.CurrentHP + "/" + this.MaxHP + "\n";

            ret += "Magic: " + this.CurrentMagic + "/" + this.MaxMagic + "\n";
            string magicMeterName;
            switch(MagicMeterLevel)
            {
                case 0: magicMeterName = "None"; break;
                case 1: magicMeterName = "Half"; break;
                case 2: magicMeterName = "Full"; break;
                default: magicMeterName = "Bugged"; break;
            }
            ret += "Magic Meter Size: " + magicMeterName + "\n";

            var spiritStones = new List<string>();
            if(this.KokiriEmerald)
                spiritStones.Add("Kokiri Emerald");
            if(this.GoronRuby)
                spiritStones.Add("Goron Ruby");
            if(this.ZoraSapphire)
                spiritStones.Add("Zora Sapphire");

            ret += "Spiritual Stones: " + string.Join(" | ", spiritStones) + "\n";

            var medallions = new List<string>();
            if(this.LightMedallion)
                medallions.Add("Light");
            if(this.ForestMedallion)
                medallions.Add("Forest");
            if(this.FireMedallion)
                medallions.Add("Fire");
            if(this.WaterMedallion)
                medallions.Add("Water");
            if(this.ShadowMedallion)
                medallions.Add("Shadow");
            if(this.SpiritMedallion)
                medallions.Add("Spirit");

            ret += "Medallions: " + string.Join(" | ", medallions) + "\n";

            var songs = new List<string>();

            if(this.ZeldasLullaby)
                songs.Add("Zelda's Lullaby");
            if(this.EponasSong)
                songs.Add("Epona's Song");
            if(this.SariasSong)
                songs.Add("Saria's Song");
            if(this.SunsSong)
                songs.Add("Sun's Song");
            if(this.SongOfTime)
                songs.Add("Song of Time");
            if(this.SongOfStorms)
                songs.Add("Song of Storms");

            var warpSongs = new List<string>();
            if(this.MinuetOfForest)
                warpSongs.Add("Minuet of Forest");
            if(this.BoleroOfFire)
                warpSongs.Add("Bolero of Fire");
            if(this.SerenadeOfWater)
                warpSongs.Add("Serenade of Water");
            if(this.NocturneOfShadow)
                warpSongs.Add("Nocturne of Shadow");
            if(this.RequiemOfSpirit)
                warpSongs.Add("Requiem of Spirit");
            if(this.PreludeOfLight)
                warpSongs.Add("Prelude of Light");

            ret += "Songs: " + string.Join(" | ", songs) + "\n";
            ret += "Songs (Warp): " + string.Join(" | ", warpSongs) + "\n";

            ret += "Gold Skulltula Count: " + this.GoldSkulltulas + "\n";
            ret += "Gerudo Membership Card: " + this.GerudoMembershipCard.ToString() + "\n";
            ret += "Stone of Agony: " + this.StoneOfAgony + "\n";

            var swords = new List<string>();
            if(this.KokiriSword)
                swords.Add("Kokiri Sword");
            if(this.MasterSword)
                swords.Add("Master Sword");
            if(this.GiantsKnife)
                swords.Add("Giant's Knife");
            if(this.BrokenGiantsKnife)
                swords.Add("Broken Giant's Knife");
            if(this.BiggoronSword)
                swords.Add("Biggoron Sword");

            ret += "Swords: " + string.Join(" | ", swords) + "\n";

            var shields = new List<string>();
            if(this.DekuShield)
                shields.Add("Deku Shield");
            if(this.HylianShield)
                shields.Add("Hylian Shield");
            if(this.MirrorShield)
                shields.Add("Mirror Shield");

            ret += "Shields: " + string.Join(" | ", shields) + "\n";

            var tunics = new List<string>();
            if(this.KokiriTunic)
                tunics.Add("Kokiri Tunic");
            if(this.GoronTunic)
                tunics.Add("Goron Tunic");
            if(this.ZoraTunic)
                tunics.Add("Zora Tunic");

            ret += "Tunics: " + string.Join(" | ", tunics) + "\n";

            var boots = new List<string>();
            if(this.KokiriBoots)
                boots.Add("Kokiri Boots");
            if(this.IronBoots)
                boots.Add("Iron Boots");
            if(this.HoverBoots)
                boots.Add("Hover Boots");

            ret += "Boots: " + string.Join(" | ", boots) + "\n\n";

            string strengthName;
            switch(this.StrengthLevel)
            {
                case 0:
                    strengthName = "None";
                    break;
                case 1:
                    strengthName = "Goron Bracelet";
                    break;
                case 2:
                    strengthName = "Silver Gauntlets";
                    break;
                case 3:
                    strengthName = "Golden Gauntlets";
                    break;
                default:
                    strengthName = "Bugged";
                    break;
            }
            ret += "Strength Upgrade: " + strengthName + "\n";

            string divingName;
            switch(this.DivingLevel)
            {
                case 0:
                    divingName = "None";
                    break;
                case 1:
                    divingName = "Silver Scale";
                    break;
                case 2:
                    divingName = "Golden Scale";
                    break;
                default:
                    divingName = "Bugged";
                    break;
            }
            ret += "Diving Level: " + divingName + "\n";

            ret += "Stick Capacity: " + this.DekuStickCapacity + "\n";
            ret += "Nut Capacity: " + this.DekuNutCapacity + "\n";
            ret += "Bomb Bag: " + this.BombCapacity + "\n";
            ret += "Seed Bag: " + this.BulletCapacity + "\n";
            ret += "Quiver: " + this.ArrowCapacity + "\n";

            var inventory = new List<string>();
            if(this.DekuStick)
                inventory.Add("Deku Sticks");
            if(this.DekuNut)
                inventory.Add("Deku Nuts");
            if(this.Bombs)
                inventory.Add("Bombs");
            if(this.Bow)
                inventory.Add("Bow");
            if(this.FireArrows)
                inventory.Add("Fire Arrows");
            if(this.DinsFire)
                inventory.Add("Din's Fire");
            if(this.Slingshot)
                inventory.Add("Slingshot");
            if(this.FairyOcarina)
                inventory.Add("Fairy Ocarina");
            if(this.OcarinaOfTime)
                inventory.Add("Ocarina of Time");
            if(this.Bombchus)
                inventory.Add("Bombchus");
            if(this.Hookshot)
                inventory.Add("Hookshot");
            if(this.Longshot)
                inventory.Add("Longshot");
            if(this.IceArrows)
                inventory.Add("Ice Arrows");
            if(this.FaroresWind)
                inventory.Add("Farore's Wind");
            if(this.Boomerang)
                inventory.Add("Boomerang");
            if(this.LensOfTruth)
                inventory.Add("Lens of Truth");
            if(this.MagicBeans)
                inventory.Add("Magic Beans");
            if(this.MegatonHammer)
                inventory.Add("Megaton Hammer");
            if(this.LightArrows)
                inventory.Add("Light Arrows");
            if(this.NayrusLove)
                inventory.Add("Nayru's Love");

            int lineBreak = 0;
            foreach(var item in inventory)
            {
                if(lineBreak % 4 == 0)
                    ret += "\n";
                lineBreak++;
                ret += item.PadLeft(20);
            }

            ret += "\nBottle Count: " + this.BottleCount + "\n";
            ret += "Child Trading Progress: " + this.ChildTrading + "\n";
            ret += "Adult Trading Progress: " + this.AdultTrading;

            return ret;
        }

        public byte[] Equipment
        {
            get
            {
                var ret = new byte[24];
                Array.Copy(this.reader.Data, this.offset + 0x0074, ret, 0, 24);
                return ret;
            }
        }

        public bool HasEquipment(byte itemId)
        {
            foreach(var byt in this.Equipment)
                if(byt == itemId)
                    return true;
            return false;
        }

        public List<byte> EquippedSlots(byte itemId)
        {
            var ret = new List<byte>();
            foreach(var byt in this.Equipment)
                if(byt == itemId)
                    ret.Add(byt);
            return ret;
        }

        public int QuantityInSlot(byte slot)
        {
            byte targetSlot = (byte)(slot % 16);
            return this.reader.Data[this.offset + 0x8C + slot];
        }

        public byte ItemInSlot(byte slotId)
        {
            byte slot = (byte)(slotId % 24);
            return this.Equipment[slot];
        }

        public byte SmallKeyCount(byte Scene)
        {
            var data = this.reader.Data[this.offset + 0x00BC + Scene];
            if(data == 0xFF)
                return 0;
            else
                return data;
        }

        public bool IsEventComplete(byte saveByte, byte saveBit)
        {
            if(saveByte > 0xD || saveByte < 0x0)
                throw new ArgumentException("Event SaveByte out-of-range");
            if(saveBit > 0xF || saveBit < 0x0)
                throw new ArgumentException("Event SaveBit out-of-range");

            return (BitConverter.ToUInt16(reader.Data, offset + 0x0ED4 + saveByte) & (1 << saveBit)) == 1;
        }

        public bool IsEventComplete(GameEvent gameEvent) => IsEventComplete(gameEvent.SaveByte, gameEvent.SaveBit);

        public bool IsItemEventComplete(byte saveByte, byte saveBit)
        {
            if(saveByte > 0x7 || saveByte < 0x0)
                throw new ArgumentException("ItemEvent SaveByte out-of-range");
            if(saveBit > 0x7 || saveBit < 0x0)
                throw new ArgumentException("ItemEvent SaveBit out-of-range");

            return ((reader.Data[offset + 0x0EF0 + saveByte]) & (1 << saveBit)) == 1;
        }

        public bool IsItemEventComplete(GameEvent gameEvent) => IsItemEventComplete(gameEvent.SaveByte, gameEvent.SaveBit);

        public bool DekuStick { get => this.HasEquipment(Items.DekuSticks); }
        public int DekuStickAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.DekuSticks);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.DekuSticks);
                return count;
            }
        }
        public bool DekuNut { get => this.HasEquipment(Items.DekuNuts); }
        public int DekuNutAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.DekuNuts);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.DekuNuts);
                return count;
            }
        }

        public bool Bombs { get => this.HasEquipment(Items.Bombs); }
        public int BombAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.Bombs);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.Bombs);
                return count;
            }
        }
        public bool Bombchus { get => this.HasEquipment(Items.Bombchus); }
        public int BombchuAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.Bombchus);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.Bombchus);
                return count;
            }
        }
        public bool HasExplosives { get => (this.Bombs || this.Bombchus); }

        public bool Slingshot { get => this.HasEquipment(Items.Slingshot); }
        public int SlingshotAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.Slingshot);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.Slingshot);
                return count;
            }
        }
        public bool Bow { get => this.HasEquipment(Items.Bow); }
        public int BowAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.Bow);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.Bow);
                return count;
            }
        }
        public bool FireArrows { get => this.HasEquipment(Items.FireArrows); }
        public bool IceArrows { get => this.HasEquipment(Items.IceArrows); }
        public bool LightArrows { get => this.HasEquipment(Items.LightArrows); }

        public bool Boomerang { get => this.HasEquipment(Items.Boomerang); }

        public bool HasOcarina { get => (this.FairyOcarina || this.OcarinaOfTime); }
        public bool FairyOcarina { get => this.HasEquipment(Items.FairyOcarina); }
        public bool OcarinaOfTime { get => this.HasEquipment(Items.OcarinaOfTime); }

        public bool HasHookshot { get => (this.Hookshot || this.Longshot); }
        public bool Hookshot { get => this.HasEquipment(Items.Hookshot); }
        public bool Longshot { get => this.HasEquipment(Items.Longshot); }

        public bool LensOfTruth { get => this.HasEquipment(Items.LensOfTruth); }

        public bool MegatonHammer { get => this.HasEquipment(Items.MegatonHammer); }

        public bool DinsFire { get => this.HasEquipment(Items.DinsFire); }
        public bool FaroresWind { get => this.HasEquipment(Items.FaroresWind); }
        public bool NayrusLove { get => this.HasEquipment(Items.NayrusLove); }

        public bool MagicBeans { get => this.HasEquipment(Items.MagicBeans); }
        public int MagicBeanAmmo
        {
            get
            {
                var count = 0;
                var slots = this.EquippedSlots(Items.MagicBeans);
                foreach(var slot in slots)
                    count += this.QuantityInSlot(Items.MagicBeans);
                return count;
            }
        }

        public ushort BottleCount
        {
            get
            {
                ushort bottleCount = 0;
                foreach(var byt in this.Equipment)
                {
                    if(byt >= Items.EmptyBottle && byt <= Items.Poe)
                        bottleCount++;
                }
                return bottleCount;
            }
        }

        public short GoldSkulltulas { get => (short)(BitConverter.ToInt16(this.reader.Data, this.offset + 0x00D0) / 256); }

        public bool DoubleDefense { get => this.reader.Data[this.offset + 0x00CF] != 0; }

        public BitmaskHelper ObtainedEquipment { get => (BitmaskHelper)BitConverter.ToUInt16(this.reader.Data, this.offset + 0x009C); }
        public BitmaskHelper ObtainedUpgrades { get => (BitmaskHelper)BitConverter.ToUInt32(this.reader.Data, this.offset + 0x00A0); }
        public BitmaskHelper QuestStatus { get => (BitmaskHelper)BitConverter.ToUInt32(this.reader.Data, this.offset + 0x00A4); }

        public bool KokiriSword { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit9); }
        public bool MasterSword { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit10); }
        public bool GiantsKnife { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit11) && !this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit12) && this.reader.Data[this.offset + 0x003E] == 0; }
        public bool BrokenGiantsKnife { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit12); }
        public bool BiggoronSword { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit11) && !this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit12) && this.reader.Data[this.offset + 0x003E] != 0; }

        public bool DekuShield { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit13); }
        public bool HylianShield { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit14); }
        public bool MirrorShield { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit15); }

        public bool KokiriTunic { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit1); }
        public bool GoronTunic { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit2); }
        public bool ZoraTunic { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit3); }

        public bool KokiriBoots { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit5); }
        public bool IronBoots { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit6); }
        public bool HoverBoots { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit7); }

        public bool GoronBracelet { get => (this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit31) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit32)); }
        public bool SilverGauntlets { get => (this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit32) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit31)); }
        public bool GoldenGuantlets { get => (this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit31) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit32)); }
        public int StrengthLevel
        {
            get
            {
                if(this.GoldenGuantlets)
                    return 3;
                if(this.SilverGauntlets)
                    return 2;
                if(this.GoronBracelet)
                    return 1;
                return 0;
            }
        }

        public bool SilverScale { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit18) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit19); }
        public bool GoldenScale { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit19) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit18); }
        public int DivingLevel
        {
            get
            {
                if(this.GoldenScale)
                    return 2;
                if(this.SilverScale)
                    return 1;
                return 0;
            }
        }

        public bool AdultWallet { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit21) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit22); }
        public bool GiantWallet { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit22) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit21); }
        public bool TycoonWallet { get => this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit21) && this.ObtainedEquipment.HasFlag(BitmaskHelper.Bit22); }
        public int RupeeCapacity
        {
            get
            {
                if(this.TycoonWallet)
                    return 999;
                if(this.GiantWallet)
                    return 500;
                if(this.AdultWallet)
                    return 200;
                return 99;
            }
        }

        public bool StickCapacity10 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit10) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit11); }
        public bool StickCapacity20 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit11) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit10); }
        public bool StickCapacity30 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit10) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit11); }
        public int DekuStickCapacity
        {
            get
            {
                if(this.StickCapacity30)
                    return 30;
                if(this.StickCapacity20)
                    return 20;
                if(this.StickCapacity10)
                    return 10;
                return 0;
            }
        }

        public bool NutCapacity20 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit13) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit14); }
        public bool NutCapacity30 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit14) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit13); }
        public bool NutCapacity40 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit13) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit14); }
        public int DekuNutCapacity
        {
            get
            {
                if(this.NutCapacity40)
                    return 40;
                if(this.NutCapacity30)
                    return 30;
                if(this.NutCapacity20)
                    return 20;
                return 0;
            }
        }

        public bool BombBag20 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit28) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit29); }
        public bool BombBag30 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit29) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit28); }
        public bool BombBag40 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit28) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit29); }
        public int BombCapacity
        {
            get
            {
                if(this.BombBag40)
                    return 40;
                if(this.BombBag30)
                    return 30;
                if(this.BombBag20)
                    return 20;
                return 0;
            }
        }

        public bool BulletBag30 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit23) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit24); }
        public bool BulletBag40 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit24) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit23); }
        public bool BulletBag50 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit23) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit24); }
        public int BulletCapacity
        {
            get
            {
                if(this.BulletBag50)
                    return 50;
                if(this.BulletBag40)
                    return 40;
                if(this.BulletBag30)
                    return 30;
                return 0;
            }
        }

        public bool Quiver30 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit25) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit26); }
        public bool Quiver40 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit26) && !this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit25); }
        public bool Quiver50 { get => this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit25) && this.ObtainedUpgrades.HasFlag(BitmaskHelper.Bit26); }
        public int ArrowCapacity
        {
            get
            {
                if(this.Quiver50)
                    return 50;
                if(this.Quiver40)
                    return 40;
                if(this.Quiver30)
                    return 30;
                return 0;
            }
        }

        public bool KokiriEmerald { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit11); }
        public bool GoronRuby { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit12); }
        public bool ZoraSapphire { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit13); }

        public bool LightMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit30); }
        public bool ForestMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit25); } // Maybe incorrect?
        public bool FireMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit26); }
        public bool WaterMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit27); }
        public bool ShadowMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit29); }
        public bool SpiritMedallion { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit28); }

        public bool ZeldasLullaby { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit21); }
        public bool EponasSong { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit22); } // maybe incorrect?
        public bool SariasSong { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit23); }
        public bool SunsSong { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit24); }
        public bool SongOfTime { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit9); } // maybe incorrect?
        public bool SongOfStorms { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit10); } // maybe incorrect?

        public bool PreludeOfLight { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit20); } // maybe incorrect?
        public bool MinuetOfForest { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit31); }
        public bool BoleroOfFire { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit32); }
        public bool SerenadeOfWater { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit17); }
        public bool NocturneOfShadow { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit19); }
        public bool RequiemOfSpirit { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit18); }

        public bool GerudoMembershipCard { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit15); }
        public bool StoneOfAgony { get => this.QuestStatus.HasFlag(BitmaskHelper.Bit14); }

        public byte ChildTrading
        {
            get
            {
                if(this.HasEquipment(Items.MaskOfTruth) || this.HasEquipment(Items.ZoraMask) || this.HasEquipment(Items.GerudoMask) || this.HasEquipment(Items.GoronMask))
                    return Items.MaskOfTruth;
                if(this.HasEquipment(Items.BunnyHood))
                    return Items.BunnyHood;
                if(this.HasEquipment(Items.SpookyMask))
                    return Items.SpookyMask;
                if(this.HasEquipment(Items.SkullMask))
                    return Items.SkullMask;
                if(this.HasEquipment(Items.KeatonMask))
                    return Items.KeatonMask;
                if(this.HasEquipment(Items.ZeldasLetter))
                    return Items.ZeldasLetter;
                if(this.HasEquipment(Items.Cucco))
                    return Items.Cucco;
                if(this.HasEquipment(Items.WeirdEgg))
                    return Items.Cucco;
                return 0x00;
            }
        }

        public byte AdultTrading
        {
            get
            {
                if(this.HasEquipment(Items.ClaimCheck))
                    return Items.ClaimCheck;
                if(this.HasEquipment(Items.EyeDrops))
                    return Items.EyeDrops;
                if(this.HasEquipment(Items.EyeballFrog))
                    return Items.EyeballFrog;
                if(this.HasEquipment(Items.Prescription))
                    return Items.Prescription;
                if(this.HasEquipment(Items.BrokenGoronSword))
                    return Items.BrokenGoronSword;
                if(this.HasEquipment(Items.PoachersSaw))
                    return Items.PoachersSaw;
                if(this.HasEquipment(Items.OddPotion))
                    return Items.OddPotion;
                if(this.HasEquipment(Items.OddMushroom))
                    return Items.OddMushroom;
                if(this.HasEquipment(Items.Cojiro))
                    return Items.Cojiro;
                if(this.HasEquipment(Items.PocketCucco))
                    return Items.PocketCucco;
                if(this.HasEquipment(Items.PocketEgg))
                    return Items.PocketEgg;
                return 0x00;
            }
        }
    }
}