﻿using System.IO;
using System.Linq;

namespace XinTracker.Zelda.OcarinaOfTime
{
    public class SaveReaderFile : IZeldaSaveData
    {
        private string fileName;

        public byte[] Data { get; protected set; }

        public byte SoundOptions { get => this.Data[0x0]; }
        public byte ZTargetOptions { get => this.Data[0x1]; }
        public byte LanguageOptions { get => this.Data[0x2]; }

        private SaveData[] saves;

        public SaveReaderFile(string fileName)
        {
            this.fileName = fileName;

            this.Data = new byte[0x8000];

            this.saves = new SaveData[3];
            this.saves[0] = new SaveData(this, 0);
            this.saves[1] = new SaveData(this, 1);
            this.saves[2] = new SaveData(this, 2);
        }

        public SaveData this[int key] { get => this.saves[key]; }

        public bool UpdateData()
        {
            var fileStream = new FileStream(this.fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var oldData = this.Data;
            this.Data = new byte[0x8000];
            fileStream.Read(this.Data, 0, 0x8000);
            //this.Data = File.ReadAllBytes(fileName);

            byte b0, b1, b2, b3;
            for(int i = 0; i < this.Data.Length; i += 4)
            {
                b0 = this.Data[i];
                b1 = this.Data[i + 1];
                b2 = this.Data[i + 2];
                b3 = this.Data[i + 3];
                this.Data[i] = b3;
                this.Data[i + 1] = b2;
                this.Data[i + 2] = b1;
                this.Data[i + 3] = b0;
            }

            return !this.Data.SequenceEqual(oldData);
        }
    }
}