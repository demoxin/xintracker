﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XinTracker.Zelda.OcarinaOfTime;

namespace XinTracker.Zelda
{
    public interface IZeldaSaveData
    {
        byte[] Data { get; }
        bool UpdateData();
        SaveData this[int key] { get; }

        byte SoundOptions { get; }
        byte ZTargetOptions { get; }
        byte LanguageOptions { get; }
    }
}
