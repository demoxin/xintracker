﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XinTracker.Zelda
{
    public struct GameEvent
    {
        public byte SaveByte;
        public byte SaveBit;

        public GameEvent(byte saveByte, byte saveBit)
        {
            this.SaveByte = saveByte;
            this.SaveBit = saveBit;
        }
    }
}
