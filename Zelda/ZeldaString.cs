﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XinTracker.Zelda
{
    public class ZeldaString
    {
        static private Dictionary<byte, char> dictionary;
        static ZeldaString()
        {
            dictionary = new Dictionary<byte, char>();

            char basA = 'A';
            for(int i = 0; i < 26; i++)
                dictionary.Add((byte)(i + 65), (char)(basA + i));
            basA = 'a';
            for(int i = 0; i < 26; i++)
                dictionary.Add((byte)(i + 91), (char)(basA + i));

            basA = '0';
            for(int i = 0; i < 10; i++)
                dictionary.Add((byte)(i + 150), (char)(basA + i));

            dictionary.Add(122, '-');
            dictionary.Add(128, '.');
            dictionary.Add(0, ' ');
        }

        public static string Build(byte[] bytes)
        {
            string ret = "";
            foreach(var byt in bytes)
            {
                if(!dictionary.ContainsKey(byt))
                    continue;
                ret += dictionary[byt];
            }
            return ret.Trim();
        }
    }
}
