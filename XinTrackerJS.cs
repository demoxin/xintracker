﻿using CefSharp.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XinTracker.Zelda.OcarinaOfTime;

namespace XinTracker
{
    public class XinTrackerJS
    {
        public Items Items = new Items();

        private SaveData saveData;

        private MainWindow window;

        public XinTrackerJS(MainWindow window)
        {
            this.window = window;
            this.window.GameDataChanged += HandleGameDataChanged;
        }

        public void HandleGameDataChanged()
        {
            this.saveData = this.window.Zelda[this.window.SaveSlot];
            this.window.Dispatcher.InvokeAsync(() => { this.window.CallRefreshGameData(); });
        }

        public string GetDetails()
        {
            if(this.saveData == null)
                return null;
            return this.saveData.ToString().Replace("\n", "<br />");
        }

        public string GetName()
        {
            if(this.saveData == null)
                return null;
            return this.saveData.Name;
        }

        public int GetWorldTime()
        {
            if(this.saveData == null)
                return -1;
            return this.saveData.WorldTime;
        }

        public int GetDeathCount()
        {
            if(this.saveData == null)
                return -1;
            return this.saveData.Deaths;
        }

        public float GetHP()
        {
            if(this.saveData == null)
                return -1f;
            return (float)Math.Round(this.saveData.CurrentHP, 2);
        }

        public int GetHeartContainers()
        {
            if(this.saveData == null)
                return -1;
            return (int)Math.Round(this.saveData.MaxHP);
        }

        public bool HasDoubleDefense()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.DoubleDefense;
        }

        public int GetMP()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.CurrentMagic;
        }

        public int GetMaxMP()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.MaxMagic;
        }

        public int GetRupees()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.Rupees;
        }

        public int GetWalletSize()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.RupeeCapacity;
        }

        public bool HasKokiriSword()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.KokiriSword;
        }

        public bool HasMasterSword()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.MasterSword;
        }

        public bool HasBiggoronSword()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.BiggoronSword;
        }

        public bool HasGiantsKnife()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.GiantsKnife;
        }

        public bool HasDekuShield()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.DekuShield;
        }

        public bool HasHylianShield()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.HylianShield;
        }

        public bool HasMirrorShield()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.MirrorShield;
        }

        public bool HasKokiriTunic()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.KokiriTunic;
        }

        public bool HasGoronTunic()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.GoronTunic;
        }

        public bool HasZoraTunic()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.ZoraTunic;
        }

        public bool HasKokiriBoots()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.KokiriBoots;
        }

        public bool HasIronBoots()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.IronBoots;
        }

        public bool HasHoverBoots()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.HoverBoots;
        }

        public int GetEquipmentInSlot(int slot)
        {
            if(this.saveData == null)
                return -1;
            if(slot > 23 || slot < 0)
                return -1;
            return this.saveData.ItemInSlot((byte)slot);
        }

        public bool HasEquipment(int itemId)
        {
            if(this.saveData == null)
                return false;
            if(itemId > Items.ClaimCheck || itemId < 0)
                return false;
            return this.saveData.HasEquipment((byte)itemId);
        }

        public bool HasDekuSticks()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.DekuStick;
        }

        public int GetDekuStickCapacity()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.DekuStickCapacity;
        }

        public int GetDekuStickCount()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.DekuStickAmmo;
        }

        public bool HasDekuNuts()
        {
            if(this.saveData == null)
                return false;
            return this.saveData.DekuNut;
        }

        public int GetDekuNutCapacity()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.DekuNutCapacity;
        }

        public int GetDekuNutCount()
        {
            if(this.saveData == null)
                return 0;
            return this.saveData.DekuNutAmmo;
        }
    }
}
